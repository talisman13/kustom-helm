#### Kustomize

within a "base" folder, there are resource definitions alongside a kustomization.yaml, which groups the resouces together. 
```
resources:
- deployment.yaml
- service.yaml
```

the app of apps pattern can be achieved by placing multiple "base" folders inside a parent folder, with a kustomization.yaml of its own. the grouping pattern is then bases instead of resources
```
bases:
- ./wordpress
- ./mysql
```

variables can be definted in the kustomization.yaml and will be ADDED to all the resources in its base, recursively (though potentially only some kinds of vars, as the syntax is specialized to the kind of var).
```
commonLabels:
  app: my-wordpress
```

changes can be made to base yaml through patches, which must be defined in a folder seperate to the "base" folder they apply to. the patch includes the bare minimum needed to identify the resources being patched, and then the changed variables
```
apiVersion: v1
kind: Service
metadata:
  name: wordpress
spec:
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: mysql
spec:
  type: NodePort
```

along side the patch, include a kustomization.yaml, that includes the relative path to the base it modifies, and the patches to be applied
```
bases:
- ../../base
patchesStrategicMerge:
- localserv.yaml
```

build the patch directory to create the modified resources
with kubectl: kubectl apply -k ./patch/folder